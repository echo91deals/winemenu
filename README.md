Wine Menu
================

Provides support for Menu entries for when Wine is installed.

## Screenshot:

![](https://imgur.com/qZEyDGl.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## Authors
- [Jerry Bezencon](https://gitlab.com/linuxlite/)
